from itertools import chain
from operator import itemgetter
from fuzzywuzzy import process
from random import choice


def speak(vector: list) -> str:
    """
    :param vector: the messages to choose from
    :return: random sentence chosen from the best candidates
    """
    limit = int(len(vector) / 10)
    sentences = candidates(vector, limit)
    return choice(sentences)


def candidates(vector: list, limit: int) -> list:
    """
    :param vector: messages to choose from
    :param limit: how many messages to choose
    :return: most common messages from the vector
    """
    words = [word for word, _ in mark_vector(split_sentences(vector))]
    return words[:limit]


def split_sentences(vector: list) -> list:
    """
    :param vector: vector of messages
    :return: flattened vector of sentences
    """
    split = [p.split(".") for p in vector]
    return [s.strip() for s in list(chain(*split)) if s != '']


def mark_vector(vector: list) -> list:
    """
    Adds a mark how much similar each word is to the rest of the vector.
    :param vector: vector to mark
    :return: List<mark, word>
    """
    marks = [mark_word(v, vector) for v in vector]
    return sorted(marks, key=itemgetter(1), reverse=True)


def mark_word(v: str, vector: list) -> (str, int):
    """
    :param v: word to mark
    :param vector: vector to mark from
    :return: (word, mark) tuple
    """
    return process.extractOne(v, [m for m in vector if m != v])
