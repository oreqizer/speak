from speak import core

# len(messages) == 10
messages = ['lol',
            'kek',
            'hi',
            'omg',
            'loll',
            'llol',
            'roflmao',
            'y u do dis',
            'pls go eat',
            'go fish']


# len(split) == 5
sentences = ['lol. kek rofl.',
             'lmao. y u. do dis']


def test_speak():
    res = core.speak(messages)
    exp = ['lol', 'loll']
    assert res in exp


def test_candidates():
    res = core.candidates(messages, 3)
    assert 'lol' in res
    assert 'loll' in res


def test_split_sentences():
    res = core.split_sentences(sentences)
    assert len(res) == 5
    assert 'lol' in res
    assert 'kek rofl' in res
    assert 'y u' in res


def test_mark_vector():
    res = core.mark_vector(messages)
    word, mark = res[0]
    word2, mark2 = res[1]
    assert len(res) == 10
    assert isinstance(mark, int)
    assert isinstance(word, str)
    assert mark2 >= mark


def test_mark_word():
    word, mark = core.mark_word('lol', messages)
    assert word != 'lol'
    assert word == 'loll'
    assert isinstance(mark, int)
